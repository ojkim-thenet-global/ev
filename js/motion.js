$(window).on('scroll', function () {
	if ($(document).scrollTop() >= 100) {
		$('header').addClass('sticky');
	}
	else {
		$('header').removeClass('sticky');
		$('.quickMenu').fadeOut();
	}
	if ($(this).scrollTop() > 600) $('.quickMenu').fadeIn();
	else $('.quickMenu').fadeOut();
});
$(document).ready(function(){
	$('.allMenu').on('click', function(e) {
		$(this).toggleClass('change');
		$('.sitemap').toggleClass('active');
		e.preventDefault();
	});
	$('.quickBtn').on('click',function(){
		$('.quickMenu').toggleClass('open');
	});
	new WOW().init();
});

var visual = function() {
	var obj = 'visual';
	var btnPlay = $('.'+obj).find('.btnPlay');
	var btnStop = $('.'+obj).find('.btnStop');
	
	btnPlay.on('click', function(){
        slide.autoplay.start();
    });
    btnStop.on('click', function(){
        slide.autoplay.stop();
	});
	var slide = new Swiper('.' + obj + ' .slide', {
		loop: true,
		speed: 800,
		autoplay: {delay: 5000},
		pagination: {
            el: '.' + obj + ' .slidePage',
            clickable: true,
            bulletClass: 'slideshow-pagination-item',
            bulletActiveClass: 'active',
            clickableClass: 'slideshow-pagination-clickable',
            modifierClass: 'slideshow-pagination-',
            renderBullet: function (index, className) {
              
              var slideIndex = index,
                  number = (index <= 8) ? '0' + (slideIndex + 1) : (slideIndex + 1);
              
              var paginationItem = '<span class="slideshow-pagination-item">';
              paginationItem += '<span class="pagination-number">' + number + '</span>';
              paginationItem = (index <= 8) ? paginationItem + '<span class="pagination-separator"><span class="pagination-separator-loader"></span></span>' : paginationItem;
              paginationItem += '</span>';
            
              return paginationItem;
              
			}
        },
        navigation: {
			prevEl: '.' + obj + ' .btnPrev',
			nextEl: '.' + obj + ' .btnNext',
		},
		on:{
			init: function(){
			  swiperAnimateCache(this);
			  swiperAnimate(this);
			}, 
			slideChangeTransitionEnd: function(){ 
			  swiperAnimate(this); 
			} 
		  }
		
	});
}


var smalSlide = function() {
	var obj = 'smalSlide';
	var slide = new Swiper('.' + obj + ' .slide', {
		loop: true,
		slidesPerView: 4.5,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
			prevEl: '.' + obj + ' .btnPrev',
			nextEl: '.' + obj + ' .btnNext',
		}
	});
}

var elevatorSlide = function() {
	var obj = 'elevatorSlide';
	var slide = new Swiper('.' + obj + ' .ovSlide', {
		loop: true,
		speed: 800,
		//autoplay: {delay: 5000},
		pagination: {
			el: '.' + obj + ' .slidePage',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span><button type="button" class="' + className + '">' + (index + 1) + '</button></span>';
			},
		},
        navigation: {
			prevEl: '.' + obj + ' .btnPrev',
			nextEl: '.' + obj + ' .btnNext',
		}
	});
}

var report = function() {
	var obj = 'report';
	var slide = new Swiper('.' + obj + ' .slide', {
		loop: true,
		slidesPerView: 4.5,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
			prevEl: '.' + obj + ' .btnPrev',
			nextEl: '.' + obj + ' .btnNext',
		}
	});
}
